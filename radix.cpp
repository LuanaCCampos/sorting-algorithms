#include<bits/stdc++.h>

using namespace std;

queue<int> fila[10]; // fila p cada algarismo

int algarismo(int num, int posicao) // função que retorna o algarismo no indice
{
	char s[3]; // num maximo de digitos

	sprintf(s, "%03d", num); // completa com zeros a esquerda * importante* ex: num = 2 -> 002

	int x = s[posicao] - '0';

	return x;	
}

int main()
{
	int i, d, n = 0, N;
	int v[100];

	d = 3; // numero de digitos

	scanf("%d", &N);

	for(i = 0; i < N; i++) // O(n)
		scanf("%d", &v[i]);

	while(d != 0) // faça p cada digito // O(d)
	{
		for(i = 0; i < N; i++) // enfilera na fila corresponde ao algarismo // O(n)
			fila[algarismo(v[i], d - 1)].push(v[i]); // inicia no algarismo menos significante

		for(i = 0; i < 10; i++) // desenfileira todas as filas  // esse for roda no máximo n + 9 = O(n) 
		{
			while(!fila[i].empty())
			{
				v[n] = fila[i].front(); //preenche vetor original
				fila[i].pop();
				n++;
			}
		}
		n = 0; // "zera" vetor
		d--;
	}

	for(i = 0; i < N; i++) // O(n)
		printf("%d ", v[i]);

	// complexidade O(n + d*2n + n) = O(n) já que d é constante

    printf("\n");
			
	return 0;
}